#! /bin/bash
install_dir="$(pwd)"
tools_dir="/fstools"

if [ $install_dir = "/" ]
then
    echo "Your are in root ($install_dir) directory. Please go to your sub directory for the wordpress installation."
else
    sh $tools_dir/bin/wp db drop --yes
    rm -R $install_dir/*

fi
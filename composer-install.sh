#! /bin/bash

install_dir="$(pwd)"
tools_dir="/fstools"
style_repo_name="foodsharing-wordpress-bezirks-website-master"
composer_repo_name="foodsharing-wordpress-bezirks-website-composer-master"

if [ $install_dir = "/" ]
then
    echo "Your are in root ($install_dir) directory. Please go to your sub directory for the wordpress installation."
else
    if test -f "$install_dir/wp-config.php" 
    then
    echo "In your wordpress directory  ($install_dir) was found a wp-config.php. That's why the script stopped.
    If you want start a fresh install use the command sh /fstools/bin/wp-clean && sh /fstools/wordpress-setup.sh"
    else
        mkdir -p $tools_dir/bin/
        curl --request GET --header 'PRIVATE-TOKEN: KJ7SqJKzJynwwRDsznyo' https://gitlab.com/foodsharing-dev/ag-bezirkswebsite/foodsharing-wordpress-bezirks-website/-/archive/master/foodsharing-wordpress-bezirks-website-master.zip -o $tools_dir/$style_repo_name.zip
        curl --request GET --header 'PRIVATE-TOKEN: KJ7SqJKzJynwwRDsznyo' https://gitlab.com/foodsharing-dev/ag-bezirkswebsite/foodsharing-wordpress-bezirks-website-composer/-/archive/master/foodsharing-wordpress-bezirks-website-composer-master.zip -o $tools_dir/$composer_repo_name.zip
        unzip -o $tools_dir/$style_repo_name.zip -d $tools_dir
        unzip -o $tools_dir/$composer_repo_name.zip -d $tools_dir
        mv -f $tools_dir/$composer_repo_name/*.sh $tools_dir/bin
        mv -f $tools_dir/$composer_repo_name/*.config $tools_dir/bin
        curl -s https://getcomposer.org/installer -o $tools_dir/bin/composer.phar
        chmod +x $tools_dir/bin/*
        cd $tools_dir/bin/
        mv -f $tools_dir/$composer_repo_name/composer.json $tools_dir/bin
        php composer.phar config extra.wordpress-install-dir --unset ; php composer.phar config extra.wordpress-install-dir "$install_dir" ;
        php composer.phar install
        cd $install_dir
        rm -R -f "$install_dir/wp-content/themes/twentynineteen" "$install_dir/wp-content/themes/twentyseventeen" "$install_dir/wp-content/themes/twentysixteen"
        mv -f $tools_dir/$style_repo_name/foodsharing-bezirks-style $install_dir/wp-content/themes
        ln -s $tools_dir/bin/vendor/wp-cli/wp-cli/bin/wp $tools_dir/bin/wp
    fi
fi
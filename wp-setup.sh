#! /bin/bash
source /fstools/bin/composer.config

adminpasswd=$(< /dev/urandom tr -dc A-Za-z0-9 | head -c14; echo)
sh $tools_dir/bin/wp core config --dbhost=$db_host --dbname=$db_name --dbuser=$db_user --dbpass=$db_pass --dbprefix=$db_prefix
sh $tools_dir/bin/wp core install --url=$site_url \
    --title="$site_titel" \
    --admin_user=$adminuser \
    --admin_password=$adminpasswd \
    --admin_email=$adminmail \
    --skip-email

if [ $install_mode != "db" ]
then
    echo "No db import"
else
        sh $tools_dir/bin/wp db drop --yes
        sh $tools_dir/bin/wp db create
        sh $tools_dir/bin/wp db import $tools_dir/$composer_repo_name/$sqlfile

        sh $tools_dir/bin/wp option update home $site_url
        sh $tools_dir/bin/wp option update siteurl $site_url
        sh $tools_dir/bin/wp user update $adminuser --user_email=$adminmail --user_pass=$adminpasswd --role=administrator --skip-email  
        sh $tools_dir/bin/wp search-replace $site_url_db $site_url --allow-root
    
fi

sh $tools_dir/bin/wp plugin install the-events-calendar --activate 
sh $tools_dir/bin/wp plugin delete akismet
sh $tools_dir/bin/wp plugin delete helo
sh $tools_dir/bin/wp theme activate foodsharing-bezirks-website
sh $tools_dir/bin/wp language core install de_DE

echo "You can login in your browser to url $site_url/wp-admin and use the user $adminuser and password $adminpasswd"
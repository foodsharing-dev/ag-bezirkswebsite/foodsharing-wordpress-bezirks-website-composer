#! /bin/bash
install_dir="$(pwd)"
tools_dir="/fstools"

if [ $install_dir = "/" ]
then
    echo "Your are in root ($install_dir) directory. Please go to your sub directory for the wordpress installation."
else
    mkdir -p $tools_dir/export
    sh $tools_dir/bin/wp db export $tools_dir/export/wp_db.sql
fi